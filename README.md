# Alliances #

## Overview ##

Alliances is a riak core powered application.
It can be run as a part of a bigger system or on its own.

### Assumptions ###
* An alliance can be updated from many actors/callers.
* The alliance data is always hot and should be kept in memory.
* We can achive high durability by
 * Use in memory replication.
 * Use data structures that can merge diverged states from different replicas of
  the data.
 * Periodically write each replica to a persistant storage.

### Implementation ###
The data type for an alliance depends heavily on [riak_dt](https://github.com/basho/riak_dt), a library that implements some [crdts](http://pagesperso-systeme.lip6.fr/Marc.Shapiro/papers/RR-6956.pdf).
For replication and distribution data of over several physical nodes we have choosen to build our application on top of [riak_core](http://basho.com/where-to-start-with-riak-core/).

## Application Structure ##

### Public API ###

* `src/alliances.erl`
  * Public API for interacting with this application.

### Request coordinators ###
Request coordinations are used to synchronize requests to the replicas of data.
* `src/alliances_get_fsm.erl`
  * Coordinator for read requests.
* `src/alliances_put_fsm.erl`
  * Coordinator for operations that changes the state of an alliance.
* `src/alliances_coverage_fsm.erl`
  * Coordinator for listing of alliance ids.
* `src/alliances_coord_util.erl`
  * Helpers for the coordinators.

### Virtual node implementation ###
In riak_core a vnode is responsible for handling a partition of the work/data.
Replication is achieved by distributing the same data over several vnodes.
* `src/alliances_vnode.erl`
  * Implementation of the riak_core_vnode behaviour.
  Read more about the riak_core vnode behaviour here:
  [Try try try: the vnode](https://github.com/rzezeski/try-try-try/tree/master/2011/riak-core-the-vnode)

### Data types ###
* `src/alliances_dt.erl`
  * Data type for one alliance.

## Test Structure

* Integration tests are in alliances_SUITE.
* Tests for internal behaviour should be in alliance_vnode_tests.
 * single vnode
 * order of internal commands
