-module(alliances_vnode_tests).

-include_lib("eunit/include/eunit.hrl").
-include("../src/alliances_vnode.hrl").

is_empty_and_create_alliance_test() ->
    State = start(),
    {true, State2} = alliances_vnode:is_empty(State),
    {ok, State3} = alliance_cmd(a, create_alliance_payload(m1), State2),
    {Alliance1, State4} = alliance_cmd(a, get_state, State3),
    ?assertEqual([m1], alliances_dt:admins(Alliance1)),
    {false, State5} = alliances_vnode:is_empty(State4),
    {{error, alliance_already_exists}, State6} =
        alliance_cmd(a, create_alliance_payload(m1), State5),
    stop(State6).

delete_alliance_test() ->
    State = start(),
    {true, State2} = alliances_vnode:is_empty(State),
    {ok, State3} = alliance_cmd(a, create_alliance_payload(m1), State2),
    {false, State4} = alliances_vnode:is_empty(State3),
    {ok, State5} = alliance_cmd(a, {delete_alliance, m1}, State4),
    {true, State6} = alliances_vnode:is_empty(State5),
    stop(State6).

delete_alliance_bad_admin_test() ->
    State = start(),
    {true, State2} = alliances_vnode:is_empty(State),
    {ok, State3} = alliance_cmd(a, create_alliance_payload(m1), State2),
    {false, State4} = alliances_vnode:is_empty(State3),
    {{error, not_admin}, State5} = alliance_cmd(a, {delete_alliance, m2}, State4),
    {false, State6} = alliances_vnode:is_empty(State5),
    stop(State6).

application_bad_admin_test() ->
    State = start(),
    {ok, State3} = alliance_cmd(a, create_alliance_payload(m1), State),
    {ok, State4} = alliance_cmd(a, add_application_payload(m2), State3),
    {{error, not_admin}, State5} = alliance_cmd(a, {accept_application, m2, m3}, State4),
    {Alliance1, State6} = alliance_cmd(a, get_state, State5),
    ?assertEqual([m2], applications(Alliance1)),
    {{error, not_admin}, State7} = alliance_cmd(a, {reject_application, m2, m3}, State6),
    {Alliance2, State8} = alliance_cmd(a, get_state, State7),
    ?assertEqual([m2], applications(Alliance2)),
    stop(State8).

application_good_test() ->
    State = start(),
    {ok, State3} = alliance_cmd(a, create_alliance_payload(m1), State),
    {ok, State4} = alliance_cmd(a, add_application_payload(m2), State3),
    {Alliance1, State5} = alliance_cmd(a, get_state, State4),
    ?assertEqual([m1], members(Alliance1)),
    ?assertEqual([m1], admins(Alliance1)),
    ?assertEqual([m2], applications(Alliance1)),
    {ok, State6} = alliance_cmd(a, {accept_application, m2, m1}, State5),
    stop(State6).

persist_test_setup() ->
    meck:new([alliances_persistance_dummy], [passthrough]).

persist_test_teardown(_) ->
    meck:unload(alliances_persistance_dummy).

persist_test_() ->
    [{setup, local,
      fun persist_test_setup/0,
      fun persist_test_teardown/1,
      [fun test_one_persist_save/0,
       fun test_one_persist_delete/0,
       fun test_one_stop/0
      ]}].

dummy_calls() ->
    {meck:num_calls(alliances_persistance_dummy, init, '_'),
     meck:num_calls(alliances_persistance_dummy, save, '_'),
     meck:num_calls(alliances_persistance_dummy, delete, '_'),
     meck:num_calls(alliances_persistance_dummy, delete_vnode, '_')}.

assert_dummy_calls({OldInit, OldSave, OldDelete, OldDeleteVnode},
                   {Init, Save, Delete, DeleteVnode}) ->
    ?assertEqual(OldInit+Init,
                 meck:num_calls(alliances_persistance_dummy, init, '_')),
    ?assertEqual(OldSave+Save,
                 meck:num_calls(alliances_persistance_dummy, save, '_')),
    ?assertEqual(OldDelete+Delete,
                 meck:num_calls(alliances_persistance_dummy, delete, '_')),
    ?assertEqual(OldDeleteVnode+DeleteVnode,
                 meck:num_calls(alliances_persistance_dummy, delete_vnode, '_')).

test_one_persist_save() ->
    D = dummy_calls(),
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    alliances_vnode:handle_info(
      {timeout, State2#state.persistence_timer_ref,
       persistence_timer},
      State2),
    stop(State2),
    assert_dummy_calls(D, {1, 1, 0, 1}).

test_one_persist_delete() ->
    D = dummy_calls(),
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    {ok, State3} = alliance_cmd(a, {delete_alliance, m1}, State2),
    stop(State3),
    assert_dummy_calls(D, {1, 0, 1, 1}).

test_one_stop() ->
    D = dummy_calls(),
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    stop(State2),
    assert_dummy_calls(D, {1, 0, 0, 1}).

promote_demote_bad_admin_test() ->
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    {ok, State3} = alliance_cmd(a, add_member_payload(m2, m1), State2),
    {{error, not_admin}, State4} = alliance_cmd(a, {promote_member, m2, m3}, State3),
    {ok, State5} = alliance_cmd(a, {promote_member, m2, m1}, State4),
    {{error, not_admin}, State6} = alliance_cmd(a, {demote_member, m2, m3}, State5),
    stop(State6).

add_remove_member_bad_admin_test() ->
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    {{error, not_admin}, State3} = alliance_cmd(a, add_member_payload(m2, m3), State2),
    {ok, State4} = alliance_cmd(a, add_member_payload(m2, m1), State3),
    {{error, not_admin}, State5} = alliance_cmd(a, {remove_member, m2, m3}, State4),
    stop(State5).

promote_demote_good_test() ->
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    {ok, State3} = alliance_cmd(a, add_member_payload(m2, m1), State2),
    {Alliance1, State4} = alliance_cmd(a, get_state, State3),
    ?assertEqual([m1], admins(Alliance1)),
    ?assertEqual([m1, m2], members(Alliance1)),
    ?assertEqual([], applications(Alliance1)),
    {ok, State5} = alliance_cmd(a, {promote_member, m2, m1}, State4),
    {Alliance2, State6} = alliance_cmd(a, get_state, State5),
    ?assertEqual([m1, m2], admins(Alliance2)),
    {ok, State8} = alliance_cmd(a, {demote_member, m2, m1}, State6),
    {Alliance4, State9} = alliance_cmd(a, get_state, State8),
    ?assertEqual([m1], admins(Alliance4)),
    {ok, State10} = alliance_cmd(a, {remove_member, m2, m1}, State9),
    {Alliance5, State11} = alliance_cmd(a, get_state, State10),
    ?assertEqual([m1], members(Alliance5)),
    stop(State11).

reject_application_good_test() ->
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    {ok, State3} = alliance_cmd(a, add_application_payload(m3), State2),
    {Alliance1, State4} = alliance_cmd(a, get_state, State3),
    ?assertEqual([m3], applications(Alliance1)),
    {ok, State5} = alliance_cmd(a, {reject_application, m3, m1}, State4),
    {Alliance2, State6} = alliance_cmd(a, get_state, State5),
    ?assertEqual([], applications(Alliance2)),
    stop(State6).

missing_alliance_test() ->
    State = start(),
    E = {error, alliance_not_existing},
    {E, State2} = alliance_cmd(a, {delete_alliance, m1}, State),
    {E, State3} = alliance_cmd(a, add_member_payload(m2, m1), State2),
    {E, State4} = alliance_cmd(a, {remove_member, m2, m1}, State3),
    {E, State5} = alliance_cmd(a, {promote_member, m2, m1}, State4),
    {E, State6} = alliance_cmd(a, {demote_member, m2, m1}, State5),
    {E, State7} = alliance_cmd(a, add_application_payload(m2), State6),
    {E, State8} = alliance_cmd(a, {accept_application, m2, m1}, State7),
    {E, State9} = alliance_cmd(a, {reject_application, m2, m1}, State8),
    {E, State10} = alliance_cmd(a, get_state, State9),
    stop(State10).

coverage_test() ->
    State = start(),
    {ok, State2} = alliance_cmd(a1, create_alliance_payload(m1), State),
    {ok, State3} = alliance_cmd(a2, create_alliance_payload(m2), State2),
    {[a1, a2], State4} = coverage(State3),
    {ok, State5} = alliance_cmd(a1, {delete_alliance, m1}, State4),
    {[a2], State6} = coverage(State5),
    stop(State6).

%% This test tries to simulate a put happening after a read before the repair.
read_put_repair_test() ->
    State = start(),
    {ok, State2} = alliance_cmd(a, create_alliance_payload(m1), State),
    {Alliance1, State3} = alliance_cmd(a, get_state, State2),
    Alliance2 = alliances_dt:add_application(Alliance1,{m3,[]},a3),
    {ok, State4} = alliance_cmd(a, add_member_payload(m2, m1), State3),
    {ok, State5} = alliance_cmd(a, {repair, Alliance2}, State4),
    {Alliance3, State6} = alliance_cmd(a, get_state, State5),
    ?assertEqual([m1], admins(Alliance3)),
    ?assertEqual([m1,m2], members(Alliance3)),
    ?assertEqual([m3], applications(Alliance3)),
    stop(State6).

%% ---------------------------------------------------------------------------
%% HELPERS

%% copied form alliances_dt_tests.
add_member_payload(UserId, AdminId) ->
    {add_member, {UserId, [{name, addedid1}]}, AdminId}.

create_alliance_payload(MemberId) ->
    {create_alliance, [{name, alliance1}], {MemberId, [{name, member1}]}}.

add_application_payload(MemberId) ->
    {add_application, {MemberId, [{name, application_user_name}]}}.

applications(A) -> lists:sort(alliances_dt:applications(A)).

admins(A) -> lists:sort(alliances_dt:admins(A)).

members(A) -> lists:sort(alliances_dt:members(A)).

start() ->
    {ok, State} = alliances_vnode:init([123]),
    State.

stop(State) ->
    {ok, NewState} = alliances_vnode:delete(State),
    NewState.

alliance_cmd(AllianceId, Operation, State) ->
    {reply, {ok, _, Reply}, State2} =
        alliances_vnode:handle_command(
          {AllianceId, alliances_coord_util:req_id(), Operation},
          mock_sender, State),
    {Reply, State2}.

coverage(State) ->
    {reply, {ok, Keys}, NewState} =
        alliances_vnode:handle_coverage(
          {alliances_coord_util:req_id(),
           {coverage, none}}, all, mocked_sender, State),
    {lists:sort(Keys), NewState}.
