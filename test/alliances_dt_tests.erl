-module(alliances_dt_tests).

-include_lib("eunit/include/eunit.hrl").

members_test() ->
    Id = <<"pelle">>,
    A0 = alliances_dt:new(Id),
    A1a = alliances_dt:add_member(A0, {m1a, []}, a1),
    A1b = alliances_dt:add_member(A0, {m1b, []}, a2),
    ?assertNot(alliances_dt:is_equal(A1a, A1b)),
    A1 = alliances_dt:merge([A1a, A1b]),
    ?assertEqual([m1a, m1b], members(A1)),
    A2a = alliances_dt:remove_member(A1a, m1a, a3),
    ?assertEqual([m1b], members(alliances_dt:merge([A2a, A1b]))).

create_test() ->
    Id = <<"a1">>,
    A0 = alliances_dt:new(Id),
    A1 = alliances_dt:create_alliance(A0, [{name, alli1}],
                                      {m1a, [{name, pelle22}]}, a1),
    ?assertEqual(members(A1), admins(A1)).

merge_test() ->
    Id = <<"a1">>,
    A0 = alliances_dt:new(Id),
    A1a = alliances_dt:add_member(A0, {m1a, []}, a1),
    A1b = alliances_dt:promote_member(A0, m1b, a1),
    A1c = alliances_dt:add_application(A0, {m1c, []}, a1),
    A1 = alliances_dt:merge([A1a, A1b, A1c]),
    ?assertEqual([m1a], members(A1)),
    ?assertEqual([m1b], admins(A1)),
    ?assertEqual([m1c], applications(A1)).

%% Some kind of crazy vector clock mumbo jumbo saves the day.
remove_merge_test() ->
    Id = <<"a1">>,
    A0 = alliances_dt:new(Id),
    A1 = alliances_dt:add_member(A0, {m1a, []}, a1),
    A2 = alliances_dt:remove_member(A1, m1a, a2),
    A3 = alliances_dt:merge([A1, A2]),
    ?assertEqual([], members(A3)).

dijoin_add_remove_merge_test() ->
    Id = <<"a1">>,
    A0 = alliances_dt:new(Id),
    A1a = alliances_dt:add_member(A0, {m1a, []}, a1),
    A1b = alliances_dt:add_member(A0, {m1a, []}, a2),
    A2b = alliances_dt:remove_member(A1b, m1a, a3),
    A3 = alliances_dt:merge([A1a, A2b]),
    ?assertEqual([m1a], members(A3)).

admin_test() ->
    Id = <<"a1">>,
    A0 = alliances_dt:new(Id),
    ?assertEqual([], admins(A0)),
    A1 = alliances_dt:add_member(A0, {m1a, []}, a1),
    A2 = alliances_dt:promote_member(A1, m1a, a2),
    ?assertEqual([m1a], admins(A2)),
    A3 = alliances_dt:demote_member(A2, m1a, a3),
    ?assertEqual([], admins(A3)).

application_test() ->
    Id = <<"a1">>,
    A0 = alliances_dt:new(Id),
    ?assertEqual([], applications(A0)),
    A1 = alliances_dt:add_application(A0, {m1a, []}, a1),
    ?assertEqual([m1a], applications(A1)),
    A2a = alliances_dt:accept_application(A1, m1a, a2),
    ?assertEqual([], applications(A2a)),
    ?assertEqual([m1a], members(A2a)),
    A2b = alliances_dt:reject_application(A1, m1a, a2),
    ?assertEqual([], applications(A2b)),
    ?assertEqual([], members(A2b)).

is_equal_test() ->
    Id = <<"a1">>,
    %% a rejected application should make trigger a merge and repair.
    A0 = alliances_dt:new(Id),
    A1 = alliances_dt:add_application(A0, {m1a, []}, a1),
    A2 = alliances_dt:reject_application(A1, m1a, a2),
    ?assertNot(alliances_dt:is_equal(A0, A2)),

    %% a added and removed member should trigger a merge and repair.
    A3 = alliances_dt:add_member(A0, {m1a, []}, a1),
    A4 = alliances_dt:remove_member(A3, m1a, a1),
    ?assertNot(alliances_dt:is_equal(A0, A4)),

    %% Order of adding users is not important, as long as actors are same.
    A5 = alliances_dt:add_member(A0, {m1a, []}, a1),
    A6 = alliances_dt:add_member(A5, {m1b, []}, a2),
    A7 = alliances_dt:add_member(A0, {m1b, []}, a2),
    A8 = alliances_dt:add_member(A7, {m1a, []}, a1),
    {S0, _} = alliances_dt:serialize(A8),
    ?assertEqual(A8, alliances_dt:deserialize(S0)),
    ?assert(alliances_dt:is_equal(A6, A8)).

applications(A) -> lists:sort(alliances_dt:applications(A)).

admins(A) -> lists:sort(alliances_dt:admins(A)).

members(A) -> lists:sort(alliances_dt:members(A)).

%% don't crash when removing something that isn't there.
%% That is ok in our data model.
remove_any_test() ->
    A = alliances_dt:new(a),
    ?assertEqual(A, alliances_dt:demote_member(A, b, 1)),
    ?assertEqual(A, alliances_dt:remove_member(A, b, 1)),
    ?assertEqual(A, alliances_dt:accept_application(A, b, 1)),
    ?assertEqual(A, alliances_dt:reject_application(A, b, 1)).
