%%%-------------------------------------------------------------------
%%% @author Andreas Hasselberg <andreas.hasselberg@gmail.com>
%%% @copyright (C) 2013, Andreas Hasselberg
%%% @doc
%%%
%%% @end
%%% Created : 11 Dec 2013 by Andreas Hasselberg <andreas.hasselberg@gmail.com>
%%%-------------------------------------------------------------------
-module(alliances_SUITE).

-compile(export_all).

-include_lib("common_test/include/ct.hrl").

%%--------------------------------------------------------------------
%% @spec suite() -> Info
%% Info = [tuple()]
%% @end
%%--------------------------------------------------------------------
suite() ->
    [{timetrap,{seconds,30}}].

%%--------------------------------------------------------------------
%% @spec init_per_suite(Config0) ->
%%     Config1 | {skip,Reason} | {skip_and_save,Reason,Config1}
%% Config0 = Config1 = [tuple()]
%% Reason = term()
%% @end
%%--------------------------------------------------------------------
init_per_suite(Config) ->
    {ok, _} = application:ensure_all_started(alliances, permanent),
    Config.

%%--------------------------------------------------------------------
%% @spec end_per_suite(Config0) -> void() | {save_config,Config1}
%% Config0 = Config1 = [tuple()]
%% @end
%%--------------------------------------------------------------------
end_per_suite(_Config) ->
    ok.

%%--------------------------------------------------------------------
%% @spec init_per_group(GroupName, Config0) ->
%%               Config1 | {skip,Reason} | {skip_and_save,Reason,Config1}
%% GroupName = atom()
%% Config0 = Config1 = [tuple()]
%% Reason = term()
%% @end
%%--------------------------------------------------------------------
init_per_group(_GroupName, Config) ->
    Config.

%%--------------------------------------------------------------------
%% @spec end_per_group(GroupName, Config0) ->
%%               void() | {save_config,Config1}
%% GroupName = atom()
%% Config0 = Config1 = [tuple()]
%% @end
%%--------------------------------------------------------------------
end_per_group(_GroupName, _Config) ->
    ok.

%%--------------------------------------------------------------------
%% @spec init_per_testcase(TestCase, Config0) ->
%%               Config1 | {skip,Reason} | {skip_and_save,Reason,Config1}
%% TestCase = atom()
%% Config0 = Config1 = [tuple()]
%% Reason = term()
%% @end
%%--------------------------------------------------------------------
init_per_testcase(_TestCase, Config) ->
    Config.

%%--------------------------------------------------------------------
%% @spec end_per_testcase(TestCase, Config0) ->
%%               void() | {save_config,Config1} | {fail,Reason}
%% TestCase = atom()
%% Config0 = Config1 = [tuple()]
%% Reason = term()
%% @end
%%--------------------------------------------------------------------
end_per_testcase(_TestCase, _Config) ->
    ok.

%%--------------------------------------------------------------------
%% @spec groups() -> [Group]
%% Group = {GroupName,Properties,GroupsAndTestCases}
%% GroupName = atom()
%% Properties = [parallel | sequence | Shuffle | {RepeatType,N}]
%% GroupsAndTestCases = [Group | {group,GroupName} | TestCase]
%% TestCase = atom()
%% Shuffle = shuffle | {shuffle,{integer(),integer(),integer()}}
%% RepeatType = repeat | repeat_until_all_ok | repeat_until_all_fail |
%%              repeat_until_any_ok | repeat_until_any_fail
%% N = integer() | forever
%% @end
%%--------------------------------------------------------------------
groups() ->
    [].

%%--------------------------------------------------------------------
%% @spec all() -> GroupsAndTestCases | {skip,Reason}
%% GroupsAndTestCases = [{group,GroupName} | TestCase]
%% GroupName = atom()
%% TestCase = atom()
%% Reason = term()
%% @end
%%--------------------------------------------------------------------
all() ->
    [my_test_case].

%%--------------------------------------------------------------------
%% @spec TestCase() -> Info
%% Info = [tuple()]
%% @end
%%--------------------------------------------------------------------
my_test_case() ->
    [].

%%--------------------------------------------------------------------
%% @spec TestCase(Config0) ->
%%               ok | exit() | {skip,Reason} | {comment,Comment} |
%%               {save_config,Config1} | {skip_and_save,Reason,Config1}
%% Config0 = Config1 = [tuple()]
%% Reason = term()
%% Comment = term()
%% @end
%%--------------------------------------------------------------------
my_test_case(_Config) ->
    Id1 = <<"Aid1">>,
    Id2 = <<"Aid2">>,
    MemberId1 = <<"Uid1">>,
    MemberId2 = <<"Uid2">>,
    MemberId3 = <<"Uid3">>,
    io:format("CREATE ALLIANCE~n"),
    alliances:create_alliance(Id1, [{name, ali1}], {MemberId1, [{name, hej}]}),
    alliances:create_alliance(Id2, [{name, ali2}], {MemberId2, []}),
    Admins = alliances:get_admins(Id1, MemberId1),
    alliances:add_member(Id1, {MemberId2, [{name, member_name2}]}, MemberId1),
    [MemberId1, MemberId2] = lists:sort(element(2, alliances:get_members(Id1))),
    io:format("PROMOTE~n"),
    alliances:promote_member(Id1, MemberId2, MemberId1),
    [MemberId1, MemberId2] = lists:sort(element(2, alliances:get_admins(Id1, MemberId2))),
    io:format("DEMOTE~n"),
    alliances:demote_member(Id1, MemberId2, MemberId1),
    Admins = alliances:get_admins(Id1, MemberId2),
    alliances:remove_member(Id1, MemberId2, MemberId1),
    {ok, [MemberId1]} = alliances:get_members(Id1),
    MemberId3 = <<"Uid3">>,
    io:format("ACCEPT~n"),
    alliances:add_application(Id1, {MemberId3, [{name, member_name3}]}),
    {ok, [MemberId3]} = alliances:get_applications(Id1, MemberId1),
    alliances:accept_application(Id1, MemberId3, MemberId1),
    {ok, []} = alliances:get_applications(Id1, MemberId1),
    [MemberId1, MemberId3] = lists:sort(element(2, alliances:get_members(Id1))),
    io:format("REJECT~n"),
    MemberId4 = <<"Uid4">>,
    alliances:add_application(Id1, {MemberId4, [{name, member_name4}]}),
    {ok, [MemberId4]} = alliances:get_applications(Id1, MemberId1),
    alliances:reject_application(Id1, MemberId4, MemberId1),
    {ok, []} = alliances:get_applications(Id1, MemberId1),
    [MemberId1, MemberId3] = lists:sort(element(2, alliances:get_members(Id1))),
    [Id1, Id2] = lists:sort(element(2, alliances:all_ids())),
    io:format("DELETE ALLIANCE~n"),
    %% FIXME: Should probably not work when several members.
    ok = alliances:delete_alliance(Id2, MemberId2),
    error = alliances:get_admins(Id2, MemberId2),
    io:format("cleanup ~n"),
    ok = alliances:delete_alliance(Id1, MemberId1),
    ok.
