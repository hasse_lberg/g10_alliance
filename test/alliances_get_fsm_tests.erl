-module(alliances_get_fsm_tests).

-include_lib("eunit/include/eunit.hrl").

one_ok_merge_test() ->
    New = alliances_dt:new(a),
    Replies = [{mocked_node1, {error, blabla}},
               {mocked_node1, New}],
    ?assertEqual({ok, New}, alliances_get_fsm:merge(Replies)).

no_replies_merge_test() ->
    ?assertEqual(error, alliances_get_fsm:merge([])).

all_error_merge_test() ->
    Replies2 = [{mocked_node1, {error, blabla}},
                {mocked_node1, {error, blabla}}],
    ?assertEqual(error, alliances_get_fsm:merge(Replies2)).

two_good_merge_test() ->
    New = alliances_dt:new(a),
    A1 = alliances_dt:add_member(New, {m1, []}, a1),
    A2 = alliances_dt:add_member(New, {m2, []}, a2),
    Replies2 = [{mocked_node1, A1},
                {mocked_node1, A2}],
    {ok, A3} = alliances_get_fsm:merge(Replies2),
    ?assertEqual([m1, m2], members(A3)).

%% ---------------------------------------------------------------------------
%% Helpers

%% stolen from alliances_dt_tests.
members(A) -> lists:sort(alliances_dt:members(A)).
