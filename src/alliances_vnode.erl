-module(alliances_vnode).

-behaviour(riak_core_vnode).

-include("alliances.hrl").
-include("alliances_vnode.hrl").
-include_lib("riak_core/include/riak_core_vnode.hrl").

-compile([{parse_transform, lager_transform}]).

%% API
-export([operation/4,
         map/3]).

%% Vnode callbacks
-export([start_vnode/1,
         init/1,
         terminate/2,
         handle_command/3,
         is_empty/1,
         delete/1,
         handle_handoff_command/3,
         handoff_starting/2,
         handoff_cancelled/1,
         handoff_finished/2,
         handle_handoff_data/2,
         encode_handoff_item/2,
         handle_coverage/4,
         handle_exit/3,
         handle_info/2]).

-ignore_xref([start_vnode/1]).

%% API
start_vnode(I) ->
    riak_core_vnode_master:get_vnode_pid(I, ?MODULE).

map(Preflist, RequestId, Operation) ->
    %% FIXME: all here?
    riak_core_vnode_master:coverage({RequestId, Operation}, Preflist,
                                    all, {fsm, undefined, self()},
                                    alliances_vnode_master).

%% @doc Create group.
operation(Preflist, RequestId, AllinaceId, Operation) ->
    riak_core_vnode_master:command(Preflist,
                                   {AllinaceId, RequestId, Operation},
                                   {fsm, undefined, self()},
                                   alliances_vnode_master).

init([Partition]) ->
    random:seed(erlang:now()), %% for the random delay of saving.
    {ok, Alliances} = persist_callback(init, [Partition]),
    Ets = ets:new(?MODULE, []),
    true = ets:insert(Ets, Alliances),
    {ok, #state{partition = Partition,
                persistence_timer_ref = persistence_timer(),
                alliances = Ets,
                node = node()}}.

%% Sample command: respond to a ping
handle_command(ping, _Sender, State) ->
    {reply, {pong, State#state.partition}, State};
handle_command({AllianceId, RequestId, {create_alliance, AllianceInfo, Member}},
               _Sender, State) ->
    lager:info("Create alliance ~p ~p~n", [Member, node()]),
    Args = [AllianceInfo, Member],
    {Reply, NewState} =
        do_create_op(AllianceId, RequestId, fun create_alliance/3, Args, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, _RequestId, {delete_alliance, AdminId}}, _Sender, State) ->
    lager:info("Delete alliance ~p ~p~n", [AllianceId, node()]),
    {Reply, NewState} = do_delete(AllianceId, AdminId, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {add_member, Member, AdminId}},
               _Sender, State) ->
    lager:info("Add member ~p ~p ~p~n", [Member, AdminId, node()]),
    {Reply, NewState} = do_admin_op(AllianceId, RequestId, fun add_member/3, Member, AdminId, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {remove_member, MemberId, AdminId}},
               _Sender, State) ->
    lager:info("Remove member ~p ~p ~p~n", [MemberId, AdminId, node()]),
    {Reply, NewState} = do_admin_op(AllianceId, RequestId, fun remove_member/3, MemberId, AdminId, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {promote_member, MemberId, AdminId}},
               _Sender, State) ->
    lager:info("Promote member ~p ~p ~p~n", [MemberId, AdminId, node()]),
    {Reply, NewState} = do_admin_op(AllianceId, RequestId, fun promote_member/3, MemberId, AdminId, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {demote_member, MemberId, AdminId}},
               _Sender, State) ->
    lager:info("Demote member ~p ~p~n", [MemberId, node()]),
    {Reply, NewState} = do_admin_op(AllianceId, RequestId, fun demote_member/3, MemberId, AdminId, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {add_application, User}},
               _Sender, State) ->
    lager:info("Add application ~p ~p~n", [User, node()]),
    {Reply, NewState} = do_op(AllianceId, RequestId, fun add_application/3, User, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {accept_application, UserId, AdminId}},
               _Sender, State) ->
    lager:info("Accept application ~p ~p ~p~n", [UserId, AdminId, node()]),
    {Reply, NewState} =
        do_admin_op(AllianceId, RequestId, fun accept_application/3, UserId, AdminId, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {reject_application, UserId, AdminId}},
               _Sender, State) ->
    lager:info("Reject application ~p ~p ~p~n", [UserId, AdminId, node()]),
    {Reply, NewState} =
        do_admin_op(AllianceId, RequestId, fun reject_application/3, UserId, AdminId, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, get_state},
               _Sender, State) ->
    lager:info("Get state ~p~n", [node()]),
    {Reply, NewState} = do_op(AllianceId, RequestId, fun get_state/3, ok, State),
    {reply, reply(Reply, NewState), NewState};
handle_command({AllianceId, RequestId, {repair, NewAlliance}},
               _Sender, State) ->
    %% FIXME: Should merge here.
    lager:info("repair ~p ~p~n", [NewAlliance, node()]),
    {Reply, NewState} = do_op(AllianceId, RequestId, fun repair_state/3, NewAlliance, State),
    {reply, reply(Reply, NewState), NewState};
handle_command(Message, _Sender, State) ->
    lager:error("unhandled_command ~p ~p~n", [Message]),
    ?PRINT({unhandled_command, Message}),
    {reply, reply({error, not_implemented}, State), State}.

handle_handoff_command(?FOLD_REQ{foldfun=FoldFun, acc0=Acc0}, _Sender, State) ->
    F = fun({Key,Val}, Acc) -> FoldFun(Key, Val, Acc) end,
    Acc = ets:foldl(F, Acc0, State#state.alliances),
    {reply, Acc, State};
handle_handoff_command({Alliance, RequestId, get_state}, _Sender, State) ->
    %% FIXME: This is untested.
    lager:info("Get state during handoff ~p~n", [node()]),
    {Reply, NewState} = do_op(Alliance, RequestId, fun get_state/3, ok, State),
    {reply, reply(Reply, NewState), NewState};
handle_handoff_command(Cmd, _Sender, State) ->
    lager:info("Forward to new vnode ~p ~p~n", [Cmd, node()]),
    {forward, State}.

handoff_starting(_TargetNode, State) ->
%%    lager:info("~p starting handoff~n~p~n", [node(), _TargetNode]),
    {true, State}.

handoff_cancelled(State) ->
    lager:info("~p cancelled ~n", [node()]),
    {ok, State}.

handoff_finished(_TargetNode, State) ->
    lager:info("~p finished ~p~n", [node(), _TargetNode]),
    {ok, State}.

handle_handoff_data(Data, State) ->
    {AllianceId, HandoffAlliance} = binary_to_term(Data),
    Alliance =
        case get_alliance(AllianceId, State) of
            {ok, LocalAlliance} ->
                alliances_dt:merge([LocalAlliance, HandoffAlliance]);
            {error, _Reason} ->
                HandoffAlliance
        end,
    lager:info("~p handoff data ~p ~p ~p~n",
              [node(), AllianceId,
               HandoffAlliance, Alliance]),
    NewAlliances = store_alliance(AllianceId, Alliance, State),
    {reply, ok, State#state{alliances = NewAlliances}}.

encode_handoff_item(ObjectName, ObjectValue) ->
    lager:info("~p encoding ~p: ~p", [node(), ObjectName, ObjectValue]),
    term_to_binary({ObjectName, ObjectValue}).

is_empty(State) ->
    case ets:info(State#state.alliances, size) of
        0 -> {true, State};
        N when is_integer(N) -> {false, State}
    end.

delete(State) ->
    true = ets:delete(State#state.alliances),
    persist_callback(delete_vnode, [State#state.partition]),
    {ok, State#state{alliances = undefined}}.

handle_coverage({_ReqId, {coverage, _Filter}}, all, _Sender, State) ->
    Keys = lists:flatten(ets:match(State#state.alliances, {'$1', '_', '_'})),
    {reply, {ok, Keys}, State}.

handle_exit(_Pid, _Reason, State) ->
    {noreply, State}.

handle_info({timeout, Ref, persistence_timer},
            #state{persistence_timer_ref = Ref} = State) ->
    save(State),
    {ok, State#state{persistence_timer_ref = persistence_timer()}}.

terminate(_Reason, _State) ->
    ok.

%% ---------------------------------------------------------------------------
%% Ops

%% alliances_dt:new(AllianceId)
create_alliance(Alliance, [AllianceInfo, Member], RequestId) ->
    {ok, alliances_dt:create_alliance(Alliance, AllianceInfo,
                                      Member, RequestId)}.

add_member(Alliance, Member, RequestId) ->
    {ok, alliances_dt:add_member(Alliance, Member, RequestId)}.

remove_member(Alliance, Member, RequestId) ->
    {ok, alliances_dt:remove_member(Alliance, Member, RequestId)}.

promote_member(Alliance, Member, RequestId) ->
    {ok, alliances_dt:promote_member(Alliance, Member, RequestId)}.

demote_member(Alliance, Member, RequestId) ->
    {ok, alliances_dt:demote_member(Alliance, Member, RequestId)}.

add_application(Alliance, User, RequestId) ->
    {ok, alliances_dt:add_application(Alliance, User, RequestId)}.

accept_application(Alliance, User, RequestId) ->
    {ok, alliances_dt:accept_application(Alliance, User, RequestId)}.

reject_application(Alliance, User, RequestId) ->
    {ok, alliances_dt:reject_application(Alliance, User, RequestId)}.

get_state(Alliance, _, _) ->
    {Alliance, Alliance}.

repair_state(OldAlliance, Alliance, _) ->
    {ok, alliances_dt:merge([Alliance, OldAlliance])}.

%% FIXME: use this for devops api.
%% set_state(_OldAlliance, Alliance, _) ->
%%     {ok, Alliance}.

%% ---------------------------------------------------------------------------
%% INTERNAL

reply(Reply, State) ->
    {ok, {State#state.partition, State#state.node}, Reply}.

do_delete(AllianceId, AdminId, State) ->
    case get_alliance(AllianceId, State) of
        {ok, Alliance} ->
            case is_admin(Alliance, AdminId) of
                true ->
                    true = ets:delete(State#state.alliances, AllianceId),
                    persist_callback(delete,
                                     [State#state.partition, AllianceId]),
                    {ok, State};
                false ->
                    {{error, not_admin}, State}
            end;
        {error, alliance_not_existing} = Err ->
            {Err, State}
    end.



do_create_op(AllianceId, RequestId, Op, Args, State) ->
    case get_alliance(AllianceId, State) of
        {ok, _Alliance} ->
            {{error, alliance_already_exists}, State};
        {error, alliance_not_existing} ->
            Alliance = alliances_dt:new(AllianceId),
            {Reply, NewAlliance} = Op(Alliance, Args, RequestId),
            {Reply, store_alliance(AllianceId, NewAlliance, State)}
    end.

do_admin_op(AllianceId, RequestId, Op, Args, AdminId, State) ->
    case get_alliance(AllianceId, State) of
        {ok, Alliance} ->
            case is_admin(Alliance, AdminId) of
                true ->
                    {Reply, NewAlliance} = Op(Alliance, Args, RequestId),
                    {Reply, store_alliance(AllianceId, NewAlliance, State)};
                false ->
                    {{error, not_admin}, State}
            end;
        {error, alliance_not_existing} = Error ->
            {Error, State}
    end.

%% FIXME: Refactor
do_op(AllianceId, RequestId, Op, Args, State) ->
    case get_alliance(AllianceId, State) of
        {ok, Alliance} ->
            {Reply, NewAlliance} = Op(Alliance, Args, RequestId),
            {Reply, store_alliance(AllianceId, NewAlliance, State)};
        {error, alliance_not_existing} = Error ->
            {Error, State}
    end.

store_alliance(AllianceId, Alliance, #state{operation_nr = Nr} = State) ->
    NewNr = Nr+1,
    true = ets:insert(State#state.alliances, [{AllianceId, NewNr, Alliance}]),
    State#state{alliances = State#state.alliances, operation_nr = NewNr}.

get_alliance(AllianceId, State) ->
    case ets:lookup(State#state.alliances, AllianceId) of
        [{AllianceId, _OperationNr, Alliance}] ->
            {ok, Alliance};
        [] ->
            {error, alliance_not_existing}
    end.

%% ---------------------------------------------------------------------------
%% timer

save(#state{operation_nr = Nr, last_save_nr = LastSaveNr,
            alliances = Alliances} = State) ->
    ets:foldl(
      fun({AllianceId, LastOpNr, Alliance}, ok) when LastOpNr >= LastSaveNr ->
              persist_callback(save,
                               [State#state.partition, AllianceId, Alliance]);
         ({_AllianceId, _LastOpNr, _Alliance}, ok) ->
              ok
      end,
      ok,
      Alliances),
    State#state{last_save_nr = Nr}.

is_admin(Alliance, AdminId) ->
    lists:member(AdminId, alliances_dt:admins(Alliance)).

persist_callback(Fun, Args) ->
    case application:get_env(alliances, persistence_cb) of
        {ok, Mod} -> ok;
        undefined -> Mod = alliances_persistance_dummy
    end,
    apply(Mod, Fun, Args).

persistence_timer() ->
    case {application:get_env(alliances, persistence_low_interval),
          application:get_env(alliances, persistence_high_interval)} of
        {{ok, LowI},
         {ok, HighI}} ->
            Interval = LowI + random:uniform(HighI - LowI),
            erlang:start_timer(Interval, self(), persistence_timer);
        {undefined, undefined} ->
            no_timer
    end.
