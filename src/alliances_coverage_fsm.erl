-module(alliances_coverage_fsm).

-include("alliances.hrl").

%% API
-export([get_all_ids/0]).

%% gen_fsm callbacks
-export([init/1,
         handle_event/3,
         handle_sync_event/4,
         handle_info/3,
         terminate/3,
         code_change/4]).

%% States
-export([prepare/2,
         execute/2,
         waiting/2]).

-record(state, {responses = 0,
                request_nodes,
                replies = gb_sets:new(),
                filter,
                req_id,
                from,
                preflist}).

get_all_ids() ->
    req(none).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

init([Filter, ReqId, Caller]) ->
    State = #state{req_id = ReqId,
                   from = Caller,
                   filter = Filter},
    {ok, prepare, State, 0}.

prepare(timeout, #state{req_id = ReqId} = State) ->
    N = ?N,
    PVC = ?N, %% FIXME: What is this?
    System = alliances, %% FIXME: I have no idea.
    {VNodes, _Other} =
        riak_core_coverage_plan:create_plan(
          allup, N, PVC, ReqId, System),
    {ok, CHash} = riak_core_ring_manager:get_my_ring(),
    {Num, _} = riak_core_ring:chash(CHash),
    {next_state, execute, State#state{request_nodes = Num,
                                      preflist = VNodes}, 0}.

%% @doc Execute the request.
execute(timeout, #state{preflist = Preflist,
                        req_id = RequestId,
                        filter = Filter} = State) ->
    alliances_vnode:map(Preflist, RequestId, {coverage, Filter}),
    {next_state, waiting, State}.

%% @doc Pull a unique list of memberships from replicas, and
%%      relay the message to it.
waiting({{undefined, _IdxNode}, {ok,Ids}},
        #state{from = From,
               req_id = ReqId,
               request_nodes = RequestNodes,
               responses = NumResponses0,
               replies = Replies0} = State0) ->
    NumResponses = NumResponses0 + 1,
    Replies = gb_sets:union(Replies0, gb_sets:from_list(Ids)),
    State = State0#state{responses=NumResponses, replies=Replies},

    case NumResponses =:= RequestNodes of
        true ->
            From ! {ReqId, ok, gb_sets:to_list(Replies)},
            {stop, normal, State};
        false ->
            {next_state, waiting, State}
    end.

%%%===================================================================

handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

handle_sync_event(_Event, _From, StateName, State) ->
    Reply = ok,
    {reply, Reply, StateName, State}.

handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.

terminate(_Reason, _StateName, _State) ->
    ok.

code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%% ---------------------------------------------------------------------------
%% Others

start_link(Filter, ReqId, Caller) ->
    Args = [Filter, ReqId, Caller],
    gen_fsm:start_link(?MODULE, Args, []).

req(Filter) ->
    ReqId = alliances_coord_util:req_id(),
    start_link(Filter, ReqId, self()),
    alliances_coord_util:wait_for_reqid(ReqId, ?TIMEOUT).
