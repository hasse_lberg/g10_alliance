-define(PRINT(Var), io:format("DEBUG: ~p:~p - ~p~n~n ~p~n~n", [?MODULE, ?LINE, ??Var, Var])).

-record(alliance,
        {id,
         members_info = vclock_dict:new(),
         alliance_info = vclock_dict:new(),
         members = riak_dt_orswot:new(),
         admins = riak_dt_orswot:new(),
         applications = riak_dt_orswot:new()}).

-define(N, 3).
-define(W, 2).
-define(R, 2).
-define(TIMEOUT, 5000).
