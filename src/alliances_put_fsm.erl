-module(alliances_put_fsm).

-behaviour(gen_fsm).

%% API
-export([create_alliance/3,
         delete_alliance/2,
         add_member/3,
         remove_member/3,
         promote_member/3,
         demote_member/3,
         add_application/2,
         accept_application/3,
         reject_application/3]).

%% gen_fsm callbacks
-export([init/1,
         handle_event/3,
         handle_sync_event/4,
         handle_info/3,
         terminate/3,
         code_change/4]).

%% States
-export([prepare/2,
         execute/2,
         waiting/2]).

-record(state, {alliance_id,
                responses = 0,
                fails = 0,
                operation,
                req_id,
                from,
                preflist}).

-include("alliances.hrl").

%%%===================================================================
%%% API
%%%===================================================================

create_alliance(AllianceId, AllianceInfo, Member) ->
    req(AllianceId, {create_alliance, AllianceInfo, Member}).

delete_alliance(AllianceId, AdminId) ->
    req(AllianceId, {delete_alliance, AdminId}).

add_member(AllianceId, Member, AdminId) ->
    req(AllianceId, {add_member, Member, AdminId}).

remove_member(AllianceId, MemberId, AdminId) ->
    req(AllianceId, {remove_member, MemberId, AdminId}).

promote_member(AllianceId, MemberId, AdminId) ->
    req(AllianceId, {promote_member, MemberId, AdminId}).

demote_member(AllianceId, MemberId, AdminId) ->
    req(AllianceId, {demote_member, MemberId, AdminId}).

add_application(AllianceId, User) ->
    req(AllianceId, {add_application, User}).

accept_application(AllianceId, UserId, AdminId) ->
    req(AllianceId, {accept_application, UserId, AdminId}).

reject_application(AllianceId, UserId, AdminId) ->
    req(AllianceId, {reject_application, UserId, AdminId}).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

init([AllianceId, Operation, ReqId, Caller]) ->
    State = #state{alliance_id = AllianceId,
                   req_id = ReqId,
                   from = Caller,
                   operation = Operation},
    {ok, prepare, State, 0}.

prepare(timeout, #state{alliance_id = AllianceId} = State) ->
    DocIdx = alliances_coord_util:chash_key(AllianceId),
    Preflist = riak_core_apl:get_apl(DocIdx, ?N, alliances),
    {next_state, execute, State#state{preflist = Preflist}, 0}.

%% @doc Execute the request.
execute(timeout, #state{preflist = Preflist,
                        operation = Operation,
                        req_id = RequestId,
                        alliance_id = AllianceId} = State) ->
    %%Preflist2 = [{Index, Node} || {{Index, Node}, _Type} <- Preflist],
    alliances_vnode:operation(Preflist, RequestId, AllianceId, Operation),
    {next_state, waiting, State}.

%% @doc Attempt to write to every single node responsible for this
%%      group.
waiting({ok, _, {error, _Reason}}, #state{fails = Fails0}=State0) ->
    maybe_reply(State0#state{fails=Fails0 + 1});
waiting({ok, _, ok}, #state{responses = Responses0}=State0) ->
    maybe_reply(State0#state{responses=Responses0 + 1}).

%%%===================================================================

handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

handle_sync_event(_Event, _From, StateName, State) ->
    Reply = ok,
    {reply, Reply, StateName, State}.

handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.

terminate(_Reason, _StateName, _State) ->
    ok.

code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

req(AllianceId, Op) ->
    ReqId = alliances_coord_util:req_id(),
    start_link(AllianceId, Op, ReqId, self()),
    alliances_coord_util:wait_for_reqid(ReqId, ?TIMEOUT).

start_link(AllianceId, Operation, ReqId, Caller) ->
    Args = [AllianceId, Operation, ReqId, Caller],
    gen_fsm:start_link(?MODULE, Args, []).

maybe_reply(#state{from = From,
                   req_id = ReqId,
                   fails = Fails,
                   responses = Responses} = State) ->
    if Responses >= ?W ->
            From ! {ReqId, ok},
            {stop, normal, State};
       Fails + Responses =:= ?N ->
            %% All complete and Responses < ?W
            From ! {ReqId, error},
            {stop, normal, State};
       Fails > (?N - ?W) ->
            %% Enough failures to fail the request.
            From ! {ReqId, error},
            {stop, normal, State};
       true ->
            {next_state, waiting, State}
    end.
