-module(alliances_coord_util).

-export([wait_for_reqid/2,
         req_id/0,
         chash_key/1]).

wait_for_reqid(ReqID, Timeout) ->
    receive
        {ReqID, ok} ->
            ok;
        {ReqID, ok, Val} ->
            {ok, Val};
        {ReqID, error} ->
            error;
        {ReqID, error, Reason} ->
            {error, Reason}
    after Timeout ->
            {error, timeout}
    end.

req_id() ->
    erlang:phash2(erlang:now()).

chash_key(AllianceId) ->
    riak_core_util:chash_key({<<"alliances">>, AllianceId}).
