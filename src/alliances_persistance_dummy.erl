-module(alliances_persistance_dummy).

-export([init/1,
         save/3,
         delete_vnode/1,
         delete/2]).

init(_Partition) ->
    %% error_logger:info_msg("Not doing init for partition ~p",
    %%                       [Partition]),
    {ok, []}.

delete_vnode(_Partition) ->
    %% error_logger:info_msg("Not deleting partition ~p",
    %%                       [Partition]),
    ok.

delete(_Partition, _AllianceId) ->
    %% error_logger:info_msg("Not deleting Alliance ~p for partition ~p",
    %%                       [AllianceId, Partition]),
    ok.

save(_Partition, _AllianceId, _Alliance) ->
    %% error_logger:info_msg("Not saving Alliance ~p ~p for partition ~p",
    %%                       [AllianceId, Alliance, Partition]),
    ok.
