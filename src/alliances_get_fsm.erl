-module(alliances_get_fsm).

-include("alliances.hrl").

%% API
-export([get_members/1,
         get_admins/2,
         get_applications/2]).

%% gen_fsm callbacks
-export([init/1,
         handle_event/3,
         handle_sync_event/4,
         handle_info/3,
         terminate/3,
         code_change/4]).

%% States
-export([prepare/2,
         execute/2,
         waiting/2,
         finalize/2,
         waiting_n/2]).

-ifdef(TEST).
-export([merge/1]).
-endif.

-record(state, {alliance_id,
                responses = 0,
                fails = 0,
                replies = [],
                merged,
                operation,
                req_id,
                from,
                preflist}).

get_members(AllianceId) ->
    req(AllianceId, fun get_members_filter/2, undefined).

get_admins(AllianceId, AdminId) ->
    req(AllianceId, fun get_admins_filter/2, AdminId).

get_applications(AllianceId, AdminId) ->
    req(AllianceId, fun get_applications_filter/2, AdminId).

%%%===================================================================
%%% gen_fsm callbacks
%%%===================================================================

init([AllianceId, Operation, ReqId, Caller]) ->
    State = #state{alliance_id = AllianceId,
                   req_id = ReqId,
                   from = Caller,
                   operation = Operation},
    {ok, prepare, State, 0}.

prepare(timeout, #state{alliance_id = AllianceId} = State) ->
    DocIdx = alliances_coord_util:chash_key(AllianceId),
    Preflist = riak_core_apl:get_apl(DocIdx, ?N, alliances),
    {next_state, execute, State#state{preflist = Preflist}, 0}.

%% @doc Execute the request.
execute(timeout, #state{preflist=Preflist,
                        req_id=RequestId,
                        operation=Operation,
                        alliance_id=AllianceId}=State) ->
%%    Preflist2 = [{Index, Node} || {{Index, Node}, _Type} <- Preflist],
    alliances_vnode:operation(Preflist, RequestId, AllianceId, Operation),
    {next_state, waiting, State}.

%% @doc Pull a unique list of memberships from replicas, and
%%      relay the message to it.
waiting({ok, IndexNode, {error, _Reason} = Reply},
        #state{fails=Fails0,
               req_id=ReqId,
               from=From,
               responses=NumResponses,
               replies=Replies0}=State0) ->
    Replies = [{IndexNode, Reply}|Replies0],
    Fails = Fails0+1,
    State = State0#state{fails=Fails, replies=Replies},
    if NumResponses + Fails =:= ?N ->
            From ! {ReqId, error},
            {next_state, finalize, State, 0};
       Fails >= (?N - ?R) ->
            From ! {ReqId, error},
            {next_state, waiting_n, State};
       true ->
            {next_state, waiting, State}
    end;
waiting({ok, IndexNode, Reply},
        #state{from=From,
               req_id=ReqId,
               fails=Fails,
               responses=NumResponses0,
               replies=Replies0}=State0) ->
    NumResponses = NumResponses0 + 1,
    Replies = [{IndexNode, Reply}|Replies0],
    State = State0#state{responses=NumResponses, replies=Replies},

    if NumResponses >= ?R ->
            {ok, MergedReply} = merge(Replies),
            From ! {ReqId, ok, MergedReply},
            case NumResponses + Fails =:= ?N of
                true ->
                    {next_state, finalize, State, 0};
                false ->
                    {next_state, waiting_n, State}
            end;
       true ->
            {next_state, waiting, State}
    end.

%% @doc Wait for the remainder of responses from replicas.
%% Don't care if error or not.
waiting_n({ok, IndexNode, Reply},
        #state{responses=NumResponses0,
               fails=Fails,
               replies=Replies0}=State0) ->
    NumResponses = NumResponses0 + 1,
    Replies = [{IndexNode, Reply}|Replies0],
    State = State0#state{responses=NumResponses, replies=Replies},

    case NumResponses + Fails =:= ?N of
        true ->
            {next_state, finalize, State, 0};
        false ->
            {next_state, waiting_n, State}
    end.

%% @doc Perform read repair.
finalize(timeout, #state{replies=Replies}=State) ->
    case merge(Replies) of
        {ok, Merged} ->
            ok = repair(Replies, State#state{merged=Merged});
        error ->
            ok
    end,
    {stop, normal, State}.

%% @doc Trigger repair if necessary.
repair([{IndexNode, {error, _Reason}}|Replies],
       #state{req_id = RequestId,
              alliance_id = AllianceId, merged=MergedAlliance}=State) ->
    alliances_vnode:operation(IndexNode, RequestId, AllianceId,
                              {repair, MergedAlliance}),
    repair(Replies, State);
repair([{IndexNode, Alliance}|Replies],
       #state{req_id = RequestId,
              alliance_id = AllianceId, merged=MergedAlliance}=State) ->
    case alliances_dt:is_equal(Alliance, MergedAlliance) of
        false ->
            alliances_vnode:operation(IndexNode, RequestId, AllianceId,
                                      {repair, MergedAlliance});
        true ->
            ok
    end,
    repair(Replies, State);
repair([], _State) -> ok.

%%%===================================================================

handle_event(_Event, StateName, State) ->
    {next_state, StateName, State}.

handle_sync_event(_Event, _From, StateName, State) ->
    Reply = ok,
    {reply, Reply, StateName, State}.

handle_info(_Info, StateName, State) ->
    {next_state, StateName, State}.

terminate(_Reason, _StateName, _State) ->
    ok.

code_change(_OldVsn, StateName, State, _Extra) ->
    {ok, StateName, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%% ---------------------------------------------------------------------------
%% Filters

get_members_filter(Alliance, _) ->
    alliances_dt:members(Alliance).

%% FIXME:
get_admins_filter(Alliance, _UserId) ->
    alliances_dt:admins(Alliance).

%% FIXME:
get_applications_filter(Alliance, _AdminId) ->
    alliances_dt:applications(Alliance).

%% ---------------------------------------------------------------------------
%% Others

start_link(AllianceId, Operation, ReqId, Caller) ->
    Args = [AllianceId, Operation, ReqId, Caller],
    gen_fsm:start_link(?MODULE, Args, []).

req(AllianceId, FilterF, CallingUserId) ->
    ReqId = alliances_coord_util:req_id(),
    start_link(AllianceId, get_state, ReqId, self()),
    case alliances_coord_util:wait_for_reqid(ReqId, ?TIMEOUT) of
        {ok, Alliance} -> {ok, FilterF(Alliance, CallingUserId)};
        Err -> Err
    end.

merge(Replies) ->
    Alliances = lists:zf(fun({_IndexNode, {error, _Reason}}) -> false;
                            ({_IndexNode, Alliance}) -> {true, Alliance}
                         end,
                         Replies),
    if Alliances =:= [] ->
            error;
       true ->
            {ok, alliances_dt:merge(Alliances)}
    end.
