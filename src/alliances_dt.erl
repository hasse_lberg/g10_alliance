-module(alliances_dt).

-include("alliances.hrl").

-export([new/1,
         create_alliance/4,
         add_member/3,
         remove_member/3,
         promote_member/3,
         demote_member/3,
         add_application/3,
         accept_application/3,
         reject_application/3,
         is_equal/2,
         members/1,
         admins/1,
         applications/1,
         merge/1,
         serialize/1,
         deserialize/1]).

-define(or_merge(Al1, Al2, Field),
        riak_dt_orswot:merge(Al1#alliance.Field, Al2#alliance.Field)).

-define(vc_merge(Al1, Al2, Field),
        vclock_dict:merge(Al1#alliance.Field, Al2#alliance.Field)).
%% ---------------------------------------------------------------------------
%% Create

new(Id) ->
    #alliance{id = Id}.

create_alliance(Alliance, AllianceInfo, {MemberId, MemberInfo} = Member, Actor) ->
    update_alliance_info(
      update_member_info(
        promote_member(add_member(Alliance, Member, Actor), MemberId, Actor),
        MemberId, MemberInfo, Actor),
      AllianceInfo, Actor).

%% ---------------------------------------------------------------------------
%% MemberInfo

update_alliance_info(#alliance{alliance_info = AI} = Alliance,
                     AllianceInfo, Actor) ->
    AI2 =
        lists:foldl(
          fun({Key, Value}, AIAcc) ->
                  vclock_dict:set(Key, Value, AIAcc, Actor)
          end,
          AI, AllianceInfo),
    Alliance#alliance{alliance_info = AI2}.

update_member_info(#alliance{members_info = MI} = Alliance,
                   MemberId, MemberInfo, Actor) ->
    MI2 = vclock_dict:set(MemberId, MemberInfo, MI, Actor),
    Alliance#alliance{members_info = MI2}.

remove_member_info(#alliance{members_info = MI} = Alliance,
                   MemberId, Actor) ->
    MI2 = vclock_dict:remove(MemberId, MI, Actor),
    Alliance#alliance{members_info = MI2}.

%% ---------------------------------------------------------------------------
%% Members

add_member(Alliance, {MemberId, MemberInfo}, Actor) ->
    NewAlliance = add_member_id(Alliance, MemberId, Actor),
    update_member_info(NewAlliance, MemberId, MemberInfo, Actor).

remove_member(Alliance, MemberId, Actor) ->
    try
        {ok, NewMembers} =
            riak_dt_orswot:update({remove, MemberId}, Actor, Alliance#alliance.members),
        remove_member_info(Alliance#alliance{members = NewMembers},
                           MemberId, Actor)
    catch error:{badmatch,{error, {precondition,{not_present,MemberId}}}} ->
            Alliance
    end.

members(#alliance{members = M1}) ->
    riak_dt_orswot:value(M1).

%% ---------------------------------------------------------------------------
%% Admin

promote_member(Alliance, Member, Actor) ->
    {ok, NewAdmins} =
        riak_dt_orswot:update({add, Member}, Actor, Alliance#alliance.admins),
    Alliance#alliance{admins = NewAdmins}.

demote_member(Alliance, Member, Actor) ->
    try
        {ok, NewAdmins} =
            riak_dt_orswot:update({remove, Member}, Actor, Alliance#alliance.admins),
        Alliance#alliance{admins = NewAdmins}
    catch error:{badmatch,{error, {precondition,{not_present,Member}}}} ->
            Alliance
            %% fixme: maybe switch to: {error, not_member}
    end.

admins(#alliance{admins = A1}) ->
    riak_dt_orswot:value(A1).

%% ---------------------------------------------------------------------------
%% Applications

add_application(Alliance, {MemberId, MemberInfo}, Actor) ->
    {ok, NewApplications} =
        riak_dt_orswot:update({add, MemberId}, Actor, Alliance#alliance.applications),
    update_member_info(Alliance#alliance{applications = NewApplications},
                       MemberId, MemberInfo, Actor).

accept_application(Alliance, MemberId, Actor) ->
    case remove_application(Alliance, MemberId, Actor) of
        {ok, NewAlliance} -> add_member_id(NewAlliance, MemberId, Actor);
        error -> Alliance
                 %% fixme: maybe switch to: {error, no_application}
    end.

reject_application(Alliance, MemberId, Actor) ->
    case remove_application(Alliance, MemberId, Actor) of
        {ok, NewAlliance} -> remove_member_info(NewAlliance, MemberId, Actor);
        error -> Alliance
                 %% fixme: maybe switch to: {error, no_application}
    end.

%% internal
add_member_id(Alliance, MemberId, Actor) ->
    {ok, NewMembers} =
        riak_dt_orswot:update({add, MemberId}, Actor, Alliance#alliance.members),
    Alliance#alliance{members = NewMembers}.


remove_application(Alliance, MemberId, Actor) ->
    try
        {ok, NewApplications} =
            riak_dt_orswot:update({remove, MemberId}, Actor, Alliance#alliance.applications),
        NewAlliance = remove_member(
                        Alliance#alliance{applications = NewApplications},
                        MemberId, Actor),
        {ok, NewAlliance}
    catch error:{badmatch,{error, {precondition,{not_present,MemberId}}}} ->
            error
    end.

applications(#alliance{applications = App1}) ->
    riak_dt_orswot:value(App1).

%% ---------------------------------------------------------------------------
%%

is_equal(#alliance{id = Id1, alliance_info = AI1,
                   members = M1, members_info = MI1,
                   admins = A1, applications = App1},
         #alliance{id = Id2, alliance_info = AI2,
                   members = M2, members_info = MI2,
                   admins = A2,  applications = App2}) ->
    (Id1 =:= Id2 andalso vclock_dict:equal(AI1, AI2) andalso
     riak_dt_orswot:equal(M1, M2) andalso vclock_dict:equal(MI1, MI2) andalso
     riak_dt_orswot:equal(A1, A2) andalso riak_dt_orswot:equal(App1, App2)).

merge(A1, A2) ->
    A2#alliance{members = ?or_merge(A1, A2, members),
                admins = ?or_merge(A1, A2, admins),
                alliance_info = ?vc_merge(A1, A2, alliance_info),
                members_info = ?vc_merge(A1, A2, members_info),
                applications = ?or_merge(A1, A2, applications)}.

merge([Alliance0 | Alliances]) ->
    lists:foldl(fun merge/2, Alliance0, Alliances).

%% ---------------------------------------------------------------------------
%% serialize / deserialize
serialize(Alliance) ->
    {term_to_binary(Alliance), "application/binary"}.

deserialize(Bin) ->
    binary_to_term(Bin).
