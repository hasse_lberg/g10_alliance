-module(vclock_dict).


-export([new/0,
         get/2,
         remove/3,
         merge/2,
         set/4,
         equal/2]).

-define(REMOVED_TOKEN, '__vclock_dict_removed__').

new() ->
    orddict:new().


get(Key, VD) ->
    case orddict:find(Key, VD) of
        {ok, {_Vclock, ?REMOVED_TOKEN}} -> error;
        {ok, {_Vclock, Value}} -> {ok, Value};
        error -> error
    end.

merge(VD1, VD2) ->
    orddict:merge(
      fun(_, {VC1, Val1}, {VC2, Val2}) ->
              case riak_dt_vclock:descends(VC2, VC1) of
                  true -> {VC2, Val2};
                  false -> {VC1, Val1}
              end
      end,
      VD1, VD2).

remove(Key, VD, Actor) ->
    case orddict:find(Key, VD) of
        {ok, {_OldVclock, ?REMOVED_TOKEN}} ->
            VD;
        {ok, {OldVclock, _OldValue}} ->
            NewVclock = riak_dt_vclock:increment(Actor, OldVclock),
            orddict:store(Key, {NewVclock, ?REMOVED_TOKEN}, VD);
        error ->
            VD
    end.

%% FIXME: Verify that this is correct.
equal(VD1, VD2) ->
    VD1 =:= VD2.

set(Key, NewValue, VD, Actor) ->
    case orddict:find(Key, VD) of
        {ok, {_OldVclock, NewValue}} ->
            VD;
        {ok, {OldVclock, _OldValue}} ->
            NewVclock = riak_dt_vclock:increment(Actor, OldVclock),
            orddict:store(Key, {NewVclock, NewValue}, VD);
        error ->
            orddict:store(Key, {riak_dt_vclock:fresh(), NewValue}, VD)
    end.

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

get_removed_test() ->
    V0 = vclock_dict:new(),
    ?assertEqual(error, vclock_dict:get(1, V0)),
    V1 = vclock_dict:set(1, 1, V0, a1),
    ?assertEqual({ok, 1}, vclock_dict:get(1, V1)),
    V2 = vclock_dict:remove(1, V1, a2),
    ?assertEqual(error, vclock_dict:get(1, V2)).

one_merge_test() ->
    VD1 = vclock_dict:set(1, 1, vclock_dict:new(), a1),
    VD2 = vclock_dict:set(1, 2, VD1, a2),
    VD3 = vclock_dict:merge(VD1, VD2),
    ?assertEqual({ok, 2}, vclock_dict:get(1, VD3)).

two_values_test() ->
    VD1 = vclock_dict:set(k1, 1, vclock_dict:new(), a1),
    VD2 = vclock_dict:set(k2, 2, VD1, a2),
    VD3 = vclock_dict:merge(VD1, VD2),
    ?assertEqual({ok, 1}, vclock_dict:get(k1, VD3)),
    ?assertEqual({ok, 2}, vclock_dict:get(k2, VD3)).

-endif.
