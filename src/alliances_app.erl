-module(alliances_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    case alliances_sup:start_link() of
        {ok, Pid} ->
            ok = riak_core:register([{vnode_module, alliances_vnode}]),
            
            check_started(riak_core_ring_events:add_guarded_handler(alliances_ring_event_handler, [])),
            check_started(riak_core_node_watcher_events:add_guarded_handler(alliances_node_event_handler, [])),
            ok = riak_core_node_watcher:service_up(alliances, self()),

            {ok, Pid};
        {error, Reason} ->
            {error, Reason}
    end.

stop(_State) ->
    ok.

check_started(ok) -> ok;
check_started({error, {allready_started, _}}) -> ok.
