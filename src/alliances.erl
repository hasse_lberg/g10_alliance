-module(alliances).

-include("alliances.hrl").
-include_lib("riak_core/include/riak_core_vnode.hrl").

-export([ping/0,
         all_ids/0,
         delete_alliance/2,
         create_alliance/3,
         add_member/3,
         remove_member/3,
         get_members/1,
         promote_member/3,
         demote_member/3,
         get_admins/2,
         add_application/2,
         accept_application/3,
         reject_application/3,
         get_applications/2]).

-ignore_xref([ping/0,
              all_ids/0,
              delete_alliance/1,
              create_alliance/3,
              add_member/2,
              remove_member/2,
              get_members/1]).

%% ---------------------------------------------------------------------------
%% API

%% @doc Pings a random vnode to make sure communication is functional
ping() ->
    DocIdx = riak_core_util:chash_key({<<"ping">>, term_to_binary(now())}),
    PrefList = riak_core_apl:get_primary_apl(DocIdx, 1, alliances),
    [{IndexNode, _Type}] = PrefList,
    riak_core_vnode_master:sync_spawn_command(IndexNode, ping, alliances_vnode_master).

all_ids() ->
    alliances_coverage_fsm:get_all_ids().

delete_alliance(AllianceId, AdminId) ->
    alliances_put_fsm:delete_alliance(AllianceId, AdminId).

create_alliance(AllianceId, AllianceInfo, {_UserId, _UserInfo} = User) ->
    alliances_put_fsm:create_alliance(AllianceId, AllianceInfo, User).

add_member(AllianceId, User, AdminId) ->
    alliances_put_fsm:add_member(AllianceId, User, AdminId).

remove_member(AllianceId, MemberId, AdminId) ->
    alliances_put_fsm:remove_member(AllianceId, MemberId, AdminId).

get_members(AllianceId) ->
    alliances_get_fsm:get_members(AllianceId).

promote_member(AllianceId, MemberId, AdminId) ->
    alliances_put_fsm:promote_member(AllianceId, MemberId, AdminId).

demote_member(AllianceId, MemberId, AdminId) ->
    alliances_put_fsm:demote_member(AllianceId, MemberId, AdminId).

get_admins(AllianceId, UserId) ->
    alliances_get_fsm:get_admins(AllianceId, UserId).

add_application(AllianceId, User) ->
    alliances_put_fsm:add_application(AllianceId, User).

accept_application(AllianceId, UserId, AdminId) ->
    alliances_put_fsm:accept_application(AllianceId, UserId, AdminId).

reject_application(AllianceId, UserId, AdminId) ->
    alliances_put_fsm:reject_application(AllianceId, UserId, AdminId).

get_applications(AllianceId, AdminId) ->
    alliances_get_fsm:get_applications(AllianceId, AdminId).
